Procedimento para criação de Lambda

Em primeiro lugar, a implantação da solução por si só não é tão complicada per se, como veríamos mais adiante no artigo. A parte complicada foi pesquisar todos os serviços disponíveis e entender como colocá-los todos juntos formando um mecanismo funcional, simples, de baixo custo, auditável e durável.

O mecanismo é composto principalmente por três serviços AWS:

· AWS Cloud Trail

· AWS Cloud Watch

· AWS Lambda

O fluxograma da solução descrito abaixo:

![](Fluxograma.png)


**Procedimento passo a passo da solução técnica**


Na próxima seção, irei guiá-lo passo a passo sobre como colocar esse mecanismo em funcionamento.


**Cloud Trail**


1. Vá para o serviço “ Trilha na nuvem ”, selecione a seção “ Trilhas ” e pressione o botão “ Criar trilha ”.

2. Deixe tudo como padrão, ao lado destes atributos:

· Aplicar trilha em todas as regiões - Escolha “não” se você quiser a trilha em apenas uma região
· Balde S3 - Selecione um nome que seja indicativo para a convenção de nomenclatura de sua organização.

3. Clique no botão “ Criar ”.

**Lambda**

1. Vá para o serviço “ Lambda ”, em “ Funções ” pressione “ Criar Função ”.

2. Deixe tudo como padrão ao lado destes atributos:

· Nome da função - Selecione um nome que seja indicativo para a convenção de nomenclatura de sua organização
· Runtime - Python 3.6

3. Na seção “ Código de função ”, cole o seguinte código

```
import boto3

def lambda_handler(event, context):

    #-------------------- Debug ---------------------------
    #print( 'Hello  {}'.format(event))
    #print( 'User Name- {}'.format(event['detail']['userIdentity']['principalId']))
    #print( 'Instance ID- {}'.format(event['detail']['responseElements']['instancesSet']['items'][0]['instanceId']))
    
    # Variables
    instanceId = event['detail']['responseElements']['instancesSet']['items'][0]['instanceId']
    userNameSTring = event['detail']['userIdentity']['principalId'] 
    
    # Checks if the user is an okta user
    if ":" in userNameSTring:
        userName  = userNameSTring.split(":")[1]
    else:
        userName  = event['detail']['userIdentity']['userName']  
        
    
    print( 'Instance Id - ' , instanceId)
    print( 'User Name - ' , userName)
    
    
    tagKey = 'owner'
    tagValue = userName
    
    # ---------------------- Body  ---------------------- 
    
    # EC2 tagging
    client = boto3.client('ec2')
    response = client.create_tags(
        Resources=[
            instanceId
        ],
        Tags=[
            {
                'Key': tagKey,
                'Value': tagValue
            },
        ]
    )
    
    # Volume tagging
    ec2 = boto3.resource('ec2')
    instance = ec2.Instance(instanceId)
    volumes = instance.volumes.all()
    for volume in volumes:
        volID = volume.id
        print("volume - " , volID)
        volume = ec2.Volume(volID)
        tag = volume.create_tags(
            Tags=[
                {
                'Key': tagKey,
                'Value': tagValue
                },
            ]
        )

    print(response)

```

4. Pressione o botão “ Salvar ”

A função Lambda precisa ter uma permissão adequada para marcar os recursos do EC2, portanto, concederemos uma política adequada para sua função predefinida.

5. Vá para o serviço “ IAM ” e pressione “ Funções ”.

6. Em “ Funções”, procure a seguinte convenção de nome de função:
“O nome da função Lambda da etapa '2'“ -role- ” sequência aleatória ”

7. clique em “ Funções ”

8. clique em “ Anexar política ”

9. procure “ AmazonEC2FullAccess ” e anexe-o à função.


**Cloud Watch**


1. Acesse o serviço “ Cloud Watch ”, em “ Eventos ” clique em “ Regras ” e pressione o botão “ criar regra ”.

2. Na seção “ Fonte do evento ”, deixe tudo como padrão ao lado destes atributos:

· Nome do serviço - EC2
· Tipo de evento - AWS API Call via Cloud Trail
· Na seção do botão de opção, selecione: " Operações específicas" e digite " RunInstances " na caixa de texto livre

3. Na seção “ Alvos ”, deixe tudo como padrão ao lado destes atributos:

· Na primeira caixa de combinação, selecione - “ Função Lambda ”
· No segundo combobox, selecione - o nome da sua função Lambda

4. Clique no botão “ Configurar detalhes ”

5. Insira um nome para sua regra

6. Clique em “ Criar regra ”


**Verifique a funcionalidade**

_Provisione uma instância EC2 e verifique se as tags estão presentes._



**Referencia**

https://medium.com/sela-devops-team/customized-resources-auto-tagging-in-aws-e3413375ceb4
